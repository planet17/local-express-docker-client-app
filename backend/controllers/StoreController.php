<?php

namespace backend\controllers;

use backend\services\PriceList\StoreListService;
use yii\web\Controller;

/**
 * Class ProductsController
 *
 * @package backend\controllers
 */
class StoreController extends Controller
{
    public function actionList(): string
    {
        return $this->render('list', ['stores' => (new StoreListService)->execute()]);
    }
}
