<?php

namespace backend\controllers;

use common\models\Databases\ActiveRecords\Stores\Product;
use common\models\Databases\ActiveRecords\Stores\StoreProductImport;
use yii\data\Pagination;
use yii\web\Controller;

/**
 * Class ProductsController
 *
 * @package backend\controllers
 */
class ProductController extends Controller
{
    public function actionStorePriceList(int $storeId): string
    {
        $pages = new Pagination(['totalCount' => Product::count(), 'pageSize' => 50]);
        return $this->render(
            'price-list',
            [
                'products' => Product::findByStoreId($storeId, $pages->limit, $pages->offset),
                'pages' => $pages
            ]);
    }
}
