<?php

namespace backend\controllers;

use backend\services\NewImportHandlerService;
use backend\services\PriceList\NewService;
use backend\services\PriceList\StoreListService;
use common\exceptions\ValidationException;
use common\forms\ImportFileForm;
use common\models\Databases\ActiveRecords\Stores\Store;
use common\models\Databases\ActiveRecords\Stores\StoreProductImport;
use common\models\Databases\ActiveRecords\Stores\StoreProductImportState;
use common\models\Forms\Stores\Products\ImportUploadForm;
use common\repositories\ActiveRecords\Import\ImportCompletedLastRepository;
use common\repositories\ActiveRecords\Import\ImportFailedLastDaysRepository;
use common\repositories\ActiveRecords\Import\ImportNewLastRepository;
use common\repositories\ActiveRecords\Import\ImportProcessingRepository;
use common\repositories\ActiveRecords\Import\ImportRepository;
use common\services\StoreProductImportListing\FecthingAndOrderingRules\PrettyComfortableStrategy;
use common\services\StoreProductImportListing\StoreProductImportListingService;
use common\services\UploadedImportFileNameGeneratorService;
use Exception;
use Yii;
use yii\data\Pagination;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\UploadedFile;

/**
 * Class ImportController
 *
 * @package backend\controllers
 */
class ImportController extends Controller
{
    public function actionDashboard(): string
    {
        $service = new StoreProductImportListingService(
            new ImportProcessingRepository,
            new ImportFailedLastDaysRepository,
            new ImportCompletedLastRepository,
            new ImportNewLastRepository
        );

        return $this->render(
            'dashboard',
            [
                'import' => $service->setFecthingAndOrderingRules(new PrettyComfortableStrategy())->makeListing(30)
            ]
        );
    }

    public function actionListing(): string
    {
        $pages = new Pagination(['totalCount' => StoreProductImport::count(), 'pageSize' => 50]);

        return $this->render(
            'listing',
            [
                'import' => (new ImportRepository)->get($pages->limit, $pages->offset),
                'pages' => $pages,
            ]
        );
    }

    public function actionStore()
    {
        try {
            $model = new ImportUploadForm();
            $service = new NewImportHandlerService(
                $model,
                new UploadedImportFileNameGeneratorService,
                Yii::$app->params['queue.service.store.productImport.parallelHandlersPerCompany']
            );
            $service->getFileNameGeneratorService()->setStorageDirectoryBasePath(getenv('DIR_CDN') ?: '/cdn');
            if(Yii::$app->request->isPost) {
                $service->handle(Yii::$app->request);

                return $this->redirect('/import/dashboard');
            }
        } catch (ValidationException $exception) {
            // nothing
        }

        return $this->render(
            'forms/upload-import',
            ['widget' => new ImportFileForm(new \yii\widgets\ActiveForm, $model)]
        );
    }
}
