<?php
/**
 * @var array[] $import
 */

use yii\helpers\Url;

?>

<div class="site-index">

  <div class="jumbotron">
    <h1>Import!</h1>

    <p class="lead">The imported files from the queue, failed imporing and the last completed importing are
      displayed here.</p>

    <p><a class="btn btn-lg btn-success" href="<?= Url::toRoute('/import/add') ?>">Add import</a></p>
  </div>
  <div class="body-content">
    <div class="row"><?= \yii\widgets\LinkPager::widget(['pagination' => $pages]) ?></div>
    <div class="row table-custom">
      <table>
        <thead>
        <tr>
          <td>Id</td>
          <td colspan="2">Store</td>
          <td colspan="2">File</td>
          <td>State</td>
          <td colspan="3">Products</td>
        </tr>
        <tr>
          <td>Id</td>
          <td>Id</td>
          <td>Name</td>
          <td>Server path</td>
          <td>Origin name</td>
          <td></td>
          <td>Imported products</td>
          <td>Fails</td>
          <td>Total</td>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($import as $row) { ?>
          <tr>
            <td><?= $row['id'] ?></td>
            <td><?= $row['store_id'] ?></td>
            <td><?= $row['store_name'] ?></td>
            <td><?= $row['file_path'] ?></td>
            <td><?= $row['file_name_origin'] ?></td>
            <td><?= $row['state_name'] ?></td>
            <td><?= $row['count_total'] == 0 ? '' : ($row['count_processing_done'] - $row['count_fail']) ?></td>
            <td><?= $row['count_total'] == 0 ? '' : $row['count_fail'] ?></td>
            <td><?= ($row['has_failed'] == true) ? 'Failed' : ($row['count_total'] == 0 ? 'Unknown' : $row['count_total']) ?></td>
          </tr>
        <?php } ?>
        </tbody>
      </table>
    </div>
    <div class="row"><?= \yii\widgets\LinkPager::widget(['pagination' => $pages]) ?></div>
  </div>
</div>