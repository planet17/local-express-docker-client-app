<?php
/**
 * @noinspection PhpFullyQualifiedNameUsageInspection
 *
 * @var \yii\widgets\ActiveForm $form
 * @var \common\models\Forms\Stores\Products\ImportUploadForm $model
 * @var \common\forms\ImportFileForm $widget
 */

//echo $widget;

$widget->begin();
echo $widget->getPropertyFileName();
echo $widget->getPropertyStore();
echo $widget->getButtonSubmit();
$widget->end();
