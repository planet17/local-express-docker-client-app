<?php
/**
 * @noinspection PhpFullyQualifiedNameUsageInspection
 *
 * @var \backend\dto\StoreForLinkWithPriceDto[]|\Generator $stores
 */

?><h1>Companies:</h1>
<ul>
    <?php foreach ($stores as $store) { ?>
      <li><?= \yii\helpers\Html::a(
              $store->getTitle() . '  ( ' . $store->getCountProducts() . ' )',
              \yii\helpers\Url::to(['product/store-price-list', 'storeId' => $store->getId()])
          ) ?></li>
    <?php } ?>
</ul>
