<?php

namespace backend\services;

use common\exceptions\ValidationException;
use common\interfaces\StoreProductImportingStatesMap;
use common\models\Databases\ActiveRecords\Stores\StoreProductImport;
use common\models\Forms\Stores\Products\ImportUploadForm;
use common\services\UploadedImportFileNameGeneratorService;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\Balancer\TaskNewDto;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Messages\ImportProcessingBalancerMessage;
use yii\web\Request;

/**
 * Class rewImportHandlerService
 *
 * @package common\services
 */
class NewImportHandlerService
{
    /** @const DIR_STORE_PRODUCTS_PATH */
    private const DIR_STORE_PRODUCTS_PATH = '/import/store/products';

    /**
     * Don't change that value without improvent `service` application.
     *
     * Must improve safety of processing for duplicates items, or racing moments such update for still not created
     * products from previous document.
     *
     * @const DIR_STORE_PRODUCTS_PATH
     */
    private const LIMIT_PARALELS_PER_STORE_DEFAULT = 1;

    /** @var ImportUploadForm $formModel */
    private $formModel;

    /** @var UploadedImportFileNameGeneratorService $fileNameGeneratorService */
    private $fileNameGeneratorService;

    /** @var int $limitDefaultParallelHandlersPerCompany */
    private $limitDefaultParallelHandlersPerCompany;

    /**
     * NewImportHandlerService constructor.
     *
     * @param ImportUploadForm $formModel
     * @param UploadedImportFileNameGeneratorService $fileNameGeneratorService
     */
    public function __construct(
        ImportUploadForm $formModel,
        UploadedImportFileNameGeneratorService $fileNameGeneratorService,
        int $limitDefaultParallelHandlersPerCompany = self::LIMIT_PARALELS_PER_STORE_DEFAULT
    ) {
        $this->formModel = $formModel;
        $this->fileNameGeneratorService = $fileNameGeneratorService;
        $this->limitDefaultParallelHandlersPerCompany = $limitDefaultParallelHandlersPerCompany;
    }

    /**
     * @return UploadedImportFileNameGeneratorService
     */
    public function getFileNameGeneratorService(): UploadedImportFileNameGeneratorService
    {
        return $this->fileNameGeneratorService;
    }

    /**
     * @param Request $request
     */
    public function handle(Request $request): void
    {
        $this->formModel->load($request->post());
        if (!$this->formModel->validate()) {
            throw new ValidationException();
        }

        foreach ($this->formModel->importFiles as $importFile) {
            $fileNameOrigin = $importFile->getBaseName() . '.' . $importFile->extension;
            $filePath = $this->fileNameGeneratorService
                ->generate(
                    $this->formModel->storeId,
                    $importFile->extension,
                    self::DIR_STORE_PRODUCTS_PATH
                );
            $importFile->saveAs($this->fileNameGeneratorService->makeFilePathFull($filePath));
            $import = new StoreProductImport;
            $import->store_id = $this->formModel->storeId;
            $import->file_name_origin = $fileNameOrigin;
            $import->file_path = $filePath;
            $import->state_id = StoreProductImportingStatesMap::IMPORT_STATE_NEW;
            $import->save();

            (new ImportProcessingBalancerMessage(
                new TaskNewDto(
                    $import->id,
                    $import->store_id,
                    $filePath,
                    /* don't change that without improvent service application */
                    $this->limitDefaultParallelHandlersPerCompany
                )
            ))->dispatch();
        }
    }
}
