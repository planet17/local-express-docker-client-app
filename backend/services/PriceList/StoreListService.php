<?php

namespace backend\services\PriceList;

use backend\dto\StoreForLinkWithPriceDto;
use common\models\Databases\ActiveRecords\Stores\Product;
use common\models\Databases\ActiveRecords\Stores\Store;
use Generator;

/**
 * Class StoreListService
 */
class StoreListService
{
    public function execute(): ?Generator
    {
        /** @var Store $store */
        foreach (Store::find()->all() as $store) {
            yield new StoreForLinkWithPriceDto($store->id, $store->title, Product::countByStore($store->id));
        }
    }
}
