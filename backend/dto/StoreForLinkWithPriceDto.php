<?php

namespace backend\dto;

/**
 * Class StoreWithPriceLinkDto
 */
class StoreForLinkWithPriceDto
{
    /** @var string $title */
    private $title;

    /** @var int $id */
    private $id;

    /** @var int $countProducts */
    private $countProducts;

    /**
     * StoreWithPriceLinkDto constructor.
     *
     * @param int $id
     * @param string $title
     * @param int $countProducts
     */
    public function __construct(int $id, string $title, int $countProducts)
    {
        $this->id = $id;
        $this->title = $title;
        $this->countProducts = $countProducts;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getCountProducts(): int
    {
        return $this->countProducts;
    }
}