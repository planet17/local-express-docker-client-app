# Local Express: Yii2 Client Application #

https://bitbucket.org/planet17/local-express-docker-client-app

Client application like administrative panel for management some processes with
stores. Here is could be adding files for importing products.


-------------------


Completed like Test Specification application.

Application based on Yii2 framework. That application is part of a few docker containers.
For more details about whole project look the repository
[Bridge](https://bitbucket.org/planet17/local-express-docker-core-bridge).


## Environment ##


-------------------

    Docker: *
    PHP >= 7.3.25
    Ext: gearman, pcntl, posix
    Gearman (PECL): >=2.0.3
    Yii 2.0.40
    Nginx: 1.19.0

-------------------


## Installation ##

After cloning that project create local `.env` and create symlink (`ln -s`)
to for `/mount/cdn/` directory at docker container.
By docker-compose your real place for link at `/.docker/mount/cdn`.
Directory for linking exist at project `Bridge` by path (`%bridge_project%/.docker/mount/cdn`).

```shell
cp .env.example .env
ln -s %current_project_path%/.docker/mount %bridge_project_path%/.docker/mount
```

If something from `.env` had been changed in another project then it must have the same value here is.
After syncing of `.env` file run following:

```shell
docker-compose up -d
docker-compose exec app bash
composer install
./yii migrate
exit
docker-compose down && docker-compose up -d
```


-------------------


DIRECTORY STRUCTURE
-------------------

```
common
    config/              contains shared configurations
    exceptions/          contains common exceptions for app
    forms/               contains forms classes for provided preconfigured widget to view
    interfaces/          contains common applcication interfaces
    mail/                contains view files for e-mails
    models/              contains model classes used in both backend and frontend
    queue/               contains queue elements such as presets, config, handlers, messages, routes
console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
backend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains backend configurations
    controllers/         contains Web controller classes
    dto/                 contains data transfer object for using between layers
    models/              contains backend-specific model classes
    runtime/             contains files generated during runtime  
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
vendor/                  contains dependent 3rd-party packages
environments/            contains environment-based overrides
```


-------------------


### Non project but also included ###

#### Domain library ####

[Dashboard for Gearman](https://bitbucket.org/planet17/local-express-common-queue-objects) - library
uses at both application (`Processing` and `Yii2`). It contains some common elements for message queue.

#### Non domain libraries but written by me for works with message queue: ####

* [https://bitbucket.org/planet17/message-queue-library](https://bitbucket.org/planet17/message-queue-library)
* [https://bitbucket.org/planet17/message-queue-library-route-nav](https://bitbucket.org/planet17/message-queue-library-route-nav)
* [https://bitbucket.org/planet17/message-queue-library-process-manager](https://bitbucket.org/planet17/message-queue-library-process-manager)
* [https://bitbucket.org/planet17/cli-processes](https://bitbucket.org/planet17/cli-processes)
* [https://bitbucket.org/planet17/application-process-manager-routed-queue](https://bitbucket.org/planet17/application-process-manager-routed-queue)
* [https://bitbucket.org/planet17/message-queue-library-pipeline-handlers](https://bitbucket.org/planet17/message-queue-library-pipeline-handlers)

