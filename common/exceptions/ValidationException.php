<?php

namespace common\exceptions;

use InvalidArgumentException;

/**
 * Class ValidationException
 *
 * @package common\exceptions
 */
class ValidationException extends InvalidArgumentException
{
}
