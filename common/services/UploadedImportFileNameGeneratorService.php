<?php

namespace common\services;

use LogicException;

/**
 * Class UploadedImportFileNameGeneratorService
 *
 * @package common\services
 */
class UploadedImportFileNameGeneratorService
{
    /** @var string $directoryPathBase */
    private $directoryPathBase = DIRECTORY_SEPARATOR;

    /**
     * @return string
     */
    public function getStorageDirectoryBasePath():string
    {
        return $this->directoryPathBase;
    }

    /**
     * @param string $directoryPathBase
     *
     * @return $this
     */
    public function setStorageDirectoryBasePath(string $directoryPathBase): UploadedImportFileNameGeneratorService
    {
        if (!file_exists($directoryPathBase)) {
            // thr new Directory not found
        }

        $this->directoryPathBase = $directoryPathBase;

        return $this;
    }

    /**
     * @param int $storeId
     * @param string $extension
     * @param string $path
     *
     * @return string
     */
    public function generate(int $storeId, string $extension, string $path = ''): string
    {
        $countSameFileName = 0;
        $fileNameBase = $storeId . '-' . date('ymdHis');

        $dirPath = $this->makeFilePath($path);
        if (!$this->isFileExists($dirPath)) {
            mkdir($dirPath, 755, true);
        }

        if (!is_dir($dirPath)) {
            // thr new Directory not found
        }

        do {
            $generatedFileName = $fileNameBase . (string)(++$countSameFileName) . '.' . $extension;
            $filePath = $this->makeFilePath($path, $generatedFileName);
        } while ($this->isFileExists($filePath));

        return $this->cleanPath($path . DIRECTORY_SEPARATOR . $generatedFileName);
    }

    /**
     * @param string $filePath
     *
     * @return string
     */
    public function makeFilePathFull(string $filePath = ''): string
    {
        return $this->makeFilePath('', $filePath);
    }

    /**
     * @param string $pathInStorage
     * @param string $fileName
     *
     * @return string
     */
    private function makeFilePath(string $pathInStorage, string $fileName = ''): string
    {
        $filePath = $this->getStorageDirectoryBasePath() . DIRECTORY_SEPARATOR . $pathInStorage . DIRECTORY_SEPARATOR . $fileName;
        return $this->cleanPath($filePath);
    }

    /**
     * @param string $filePath
     *
     * @return bool
     */
    private function isFileExists(string $filePath): bool
    {
        return file_exists($filePath);
    }

    /**
     * @param $path
     *
     * @return mixed
     */
    private function cleanPath($path)
    {
        while (strpos($path, str_repeat(DIRECTORY_SEPARATOR, 2)) !== false) {
            $path = str_replace(str_repeat(DIRECTORY_SEPARATOR, 2), DIRECTORY_SEPARATOR, $path);
        }

        return $path;
    }
}
