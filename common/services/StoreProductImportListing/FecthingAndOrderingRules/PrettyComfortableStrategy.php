<?php

namespace common\services\StoreProductImportListing\FecthingAndOrderingRules;

use common\services\StoreProductImportListing\FecthingAndOrderingRulesStrategyInterface;
use common\repositories\ImportRepositoryInterface;

/**
 * Class PrettyComfortableStrategy
 *
 * @package common\services\StoreProductImportListing\FecthingAndOrderingRules
 */
class PrettyComfortableStrategy implements FecthingAndOrderingRulesStrategyInterface
{
    /** @inheritDoc */
    public function execute(
        int $limit,
        ImportRepositoryInterface $repositoryProcessing,
        ImportRepositoryInterface $repositoryFailedLastDays,
        ImportRepositoryInterface $repositoryCompletedLastHours,
        ImportRepositoryInterface $repositoryNewLastHours
    ): array {
        $result = $repositoryProcessing->get($limit);
        $limit -= count($result);
        if ($limit < 1) {
            return $result;
        }

        if ($rows = $repositoryFailedLastDays->get($limit)) {
            $limit -= count($rows);
            $result = array_merge($result, $rows);
        }

        if ($limit < 1) {
            return $result;
        }

        $new = $repositoryNewLastHours->get($limit);
        $last = $repositoryCompletedLastHours->get($limit);

        if (!count($last) && !count($new)) {
            return $result;
        }

        if (!count($last) && count($new)) {
            return array_merge($result, $new);
        }

        if (count($last) && !count($new)) {
            return array_merge($result, $last);
        }

        if ((count($last) + count($new)) <= $limit) {
            return array_merge($result, $new, $last);
        }

        if ((count($last) >= $limit || count($new) >= $limit) && $limit > 1) {
            $countraExtra = $limit - (count($last) + count($new));
            for (; true;) {
                array_pop($new);
                if (--$countraExtra < 1) {
                    break;
                }
                array_pop($last);
                if (--$countraExtra < 1) {
                    break;
                }
                array_pop($new);
                if (--$countraExtra < 1) {
                    break;
                }
            }
        }

        return array_merge($result, $new, $last);
    }
}
