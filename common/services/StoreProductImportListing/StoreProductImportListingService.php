<?php

namespace common\services\StoreProductImportListing;

use common\repositories\ImportRepositoryInterface;

/**
 * Class StoreProductImportListingService
 *
 * @package common\services\StoreProductImportListing
 */
class StoreProductImportListingService
{
    /** @var ImportRepositoryInterface */
    private $repositoryProcessing;

    /** @var ImportRepositoryInterface */
    private $repositoryFailedLast;

    /** @var ImportRepositoryInterface */
    private $repositoryCompletedLast;

    /** @var ImportRepositoryInterface */
    private $repositoryNewLast;

    /** @var FecthingAndOrderingRulesStrategyInterface */
    private $strategy;

    /**
     * StoreProductImportListingService constructor.
     *
     * @param ImportRepositoryInterface $repositoryProcessing
     * @param ImportRepositoryInterface $repositoryFailedLast
     * @param ImportRepositoryInterface $repositoryCompletedLast
     * @param ImportRepositoryInterface $repositoryNewLast
     */
    public function __construct(
        ImportRepositoryInterface $repositoryProcessing,
        ImportRepositoryInterface $repositoryFailedLast,
        ImportRepositoryInterface $repositoryCompletedLast,
        ImportRepositoryInterface $repositoryNewLast
    ) {
        $this->repositoryProcessing = $repositoryProcessing;
        $this->repositoryFailedLast = $repositoryFailedLast;
        $this->repositoryCompletedLast = $repositoryCompletedLast;
        $this->repositoryNewLast = $repositoryNewLast;
    }

    /**
     * @param FecthingAndOrderingRulesStrategyInterface $strategy
     */
    public function setFecthingAndOrderingRules(FecthingAndOrderingRulesStrategyInterface $strategy): self
    {
        $this->strategy = $strategy;

        return $this;
    }

    /**
     * @param $limit
     *
     * @return array
     */
    public function makeListing($limit): array
    {
        return $this->strategy->execute(
            $limit,
            $this->repositoryProcessing,
            $this->repositoryFailedLast,
            $this->repositoryCompletedLast,
            $this->repositoryNewLast
        );
    }
}
