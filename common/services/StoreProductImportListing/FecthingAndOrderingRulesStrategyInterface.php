<?php

namespace common\services\StoreProductImportListing;

use common\repositories\ImportRepositoryInterface;

/**
 * Interface FecthingAndOrderingRulesStrategyInterface
 *
 * @package common\services\StoreProductImportListing
 */
interface FecthingAndOrderingRulesStrategyInterface
{
    /**
     * @param int $limit
     * @param ImportRepositoryInterface $repositoryProcessing
     * @param ImportRepositoryInterface $repositoryFailedLastDays
     * @param ImportRepositoryInterface $repositoryCompleted
     * @param ImportRepositoryInterface $repositoryNewLastHoursLastHours
     *
     * @return array
     */
    public function execute(
        int $limit,
        ImportRepositoryInterface $repositoryProcessing,
        ImportRepositoryInterface $repositoryFailedLastDays,
        ImportRepositoryInterface $repositoryCompleted,
        ImportRepositoryInterface $repositoryNewLastHoursLastHours
    ): array;
}
