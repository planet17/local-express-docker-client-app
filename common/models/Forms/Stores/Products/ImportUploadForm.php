<?php

namespace common\models\Forms\Stores\Products;

use common\models\Databases\ActiveRecords\Stores\Store;
use common\services\UploadedImportFileNameGeneratorService;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class ImportAddForm
 *
 * @package common\models
 */
class ImportUploadForm extends Model
{
    /** @const PROPERTY_NAME_FILE */
    public const PROPERTY_NAME_FILE = 'importFiles';

    /** @const PROPERTY_NAME_STORE */
    public const PROPERTY_NAME_STORE = 'storeId';

    public $storeId;

    /** @var UploadedFile[] */
    public $importFiles;

    private $allowedExtension =  ['csv', 'xls', 'xlsx'];

    public function load($data, $formName = null)
    {
        if (!parent::load($data, $formName)) {
            return false;
        }

        $this->importFiles = UploadedFile::getInstances($this, $this::PROPERTY_NAME_FILE);

        return (bool)$this->importFiles;
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        //$extensionString = 'csv, xls, xslsx';
        //$extensionArr = ['csv', 'xls', 'xlsx', 'text/csv', 'application/csv', 'text/comma-separated-values',
        // 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/anytext', 'application/vnd', 'ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];

        return [
            [
                [self::PROPERTY_NAME_STORE, self::PROPERTY_NAME_FILE],
                'required'
            ],
            [
                self::PROPERTY_NAME_STORE,
                'integer',
            ],
            [
                [self::PROPERTY_NAME_FILE],
                'file',
                'skipOnEmpty' => false,
//                'extensions' => $extensionArr,
                'maxFiles' => 4,
                'maxSize' => 5000000,
                'tooBig' => 'Limit is 5MB'
            ],
            [self::PROPERTY_NAME_STORE, 'exist', 'targetClass' => Store::class, 'targetAttribute' => [self::PROPERTY_NAME_STORE => 'id']],
        ];
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        if (!parent::validate($attributeNames, $clearErrors)) {
            return false;
        }

        foreach ($this->importFiles as $file) {
            if (!in_array($file->extension, $this->allowedExtension)) {
                $error = 'Allowed extension extension is only: ' . implode(', ', $this->allowedExtension);
                $this->addError(self::PROPERTY_NAME_FILE, $error);
                return false;
            }
        }

        return true;
    }

    public function getWidgetConfiguration(): array
    {
        return ['options' => ['enctype' => 'multipart/form-data']];
    }

    public function getDropdownOptionStores()
    {
        $options = Store::find()->select(['title', 'id'])->indexBy('id')->column();
        $options[1919] = 'Sandy and Crubs';

        return $options;
    }
}
