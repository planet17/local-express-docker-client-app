<?php

namespace common\models\Databases\ActiveRecords\Stores;

use yii\db\ActiveRecord;

/**
 * User model
 *
 * @property int $id
 * @property int $store_id
 * @property string $title
 * @property string $upc
 * @property float $price
 */
class Product extends ActiveRecord
{
    /** @inheritdoc */
    public static function tableName(): string
    {
        return '{{%store_products}}';
    }

    /**
     * @param $id
     *
     * @return Product|null
     */
    public static function findByProductId($id): ?Product
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * @param int $storeId
     * @param int $limit
     * @param int $offset
     *
     * @return array
     */
    public static function findByStoreId(int $storeId, int $limit, int $offset = 0): array
    {
        return parent::find()
            ->where(['store_id' => $storeId])
            ->orderBy('upc')
            ->limit($limit)
            ->offset($offset)
            ->all();
    }

    /**
     * @param int $storeId
     *
     * @return int
     */
    public static function countByStore(int $storeId): int
    {
        return parent::find()
            ->where(['store_id' => $storeId])
            ->count();
    }

    /**
     * Find product by upc.
     *
     * @param int $storeId
     * @param string $upc
     *
     * @return static|null
     */
    public static function findByUpc(int $storeId, string $upc): ?Product
    {
        return static::findOne(['store_id' => $storeId, 'upc' => $upc]);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->getPrimaryKey();
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public static function count(): int
    {
        return static::find()->select('COUNT(*) AS cnt')->asArray()->one()['cnt'] ?? 0;
    }
}
