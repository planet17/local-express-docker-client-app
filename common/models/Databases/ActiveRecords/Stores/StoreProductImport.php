<?php

namespace common\models\Databases\ActiveRecords\Stores;

use common\interfaces\StoreProductImportingStatesMap;
use yii\db\ActiveRecord;

/**
 * Class StoreProductImport
 *
 * @property int $id
 * @property string $file_path
 * @property string $file_name_origin
 * @property int $store_id
 * @property int $state_id
 * @property bool $has_file_been_read
 * @property bool $has_failed
 * @property int $count_total
 * @property int $count_fail
 * @property int $count_processing_done
 *
 * @property string $created_at
 * @property string $done_at
 * @property string $updated_at
 */
class StoreProductImport extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%store_product_import}}';
    }

    /**
     * Method predicate for determine whether import can be changed to state `done`.
     *
     * @return bool
     */
    public function isCouldBeChangedToStateDone(): bool
    {
        if ($this->has_failed) {
            return true;
        }

        if (!$this->has_file_been_read) {
            return false;
        }

        if (!$this->count_total || !$this->count_processing_done) {
            return false;
        }

        return ((int)$this->count_total - (int)$this->count_processing_done) === 0;
    }

    public function changeStateFailed()
    {
        $this->has_failed = true;
        $this->changeStateDone();
    }

    public function changeStateProcessing()
    {
        $hasntStillProcessing = (int)$this->state_id < StoreProductImportingStatesMap::IMPORT_STATE_PROCESSING;
        if ($hasntStillProcessing) {
            $this->state_id = StoreProductImportingStatesMap::IMPORT_STATE_PROCESSING;
        }
    }

    public function changeStateDoneByProduct()
    {
        $this->changeStateDoneCommon();
    }

    public function changeStateDone()
    {
        $this->changeStateDoneCommon(true);
    }

    private function changeStateDoneCommon(bool $isFromReader = false)
    {
        $this->changeStateProcessing();
        $this->has_file_been_read = true;

        $hasntStillDone = (int)$this->state_id < StoreProductImportingStatesMap::IMPORT_STATE_DONE;
        if ($hasntStillDone && $this->isCouldBeChangedToStateDone()) {
            $this->state_id = StoreProductImportingStatesMap::IMPORT_STATE_DONE;
            $this->done_at = date('Y-m-d H:i:s');
        }
    }

    /**
     * @param int $increment
     */
    public function incrementDone(int $increment = 1): void
    {
        $this->updateCounters(['count_processing_done' => $increment]);
    }

    /**
     * @param int $increment
     */
    public function incrementFail(int $increment = 1): void
    {
        $this->updateCounters(['count_fail' => $increment]);
    }

    /**
     * @return int
     */
    public static function count(): int
    {
        return static::find()->select('COUNT(*) AS cnt')->asArray()->one()['cnt'] ?? 0;
    }
}
