<?php

namespace common\models\Databases\ActiveRecords\Stores;

use yii\db\ActiveRecord;

/**
 * Class Stores
 *
 * @property int $id
 * @property string $title
 */
class Store extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%stores}}';
    }
}
