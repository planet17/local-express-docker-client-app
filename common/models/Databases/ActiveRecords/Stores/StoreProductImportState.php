<?php

namespace common\models\Databases\ActiveRecords\Stores;

use yii\db\ActiveRecord;

/**
 * Class StoreProductImport
 *
 * @property int $id
 * @property string $file_path
 * @property int $store_id
 * @property int $state_id
 * @property bool $has_file_been_read
 * @property int $count_total
 * @property int $count_fail
 * @property int $count_processing_done
 */
class StoreProductImportState extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%store_product_import_states}}';
    }
}
