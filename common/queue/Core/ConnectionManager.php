<?php

namespace common\queue\Core;

use Planet17\MessageQueueLibrary\Connections\ConnectionFactory;
use Planet17\MessageQueueLibrary\Interfaces\Providers\RoutesProviderInterface;
use Planet17\MessageQueueLibrary\Interfaces\Resolvers\ConnectionResolverInterface;
use Planet17\MessageQueueLibrary\Interfaces\Resolvers\RouteResolverInterface;
use Planet17\MessageQueueLibrary\Resolvers\ConnectionResolver;
use Planet17\MessageQueueLibrary\Resolvers\RouteInstanceResolver;
use Planet17\MessageQueueLibraryRouteNav\Connections\ConnectionManagerBase;
use Planet17\MessageQueueLibraryRouteNav\Interfaces\Resolvers\AliasHandlerResolverInterface;

/**
 * Class ConnectionManager
 *
 * @package common\core\Queue
 */
class ConnectionManager extends ConnectionManagerBase
{
    /** @inheritdoc  */
    public function getResolverAliasHandler(): AliasHandlerResolverInterface
    {
        throw new \RuntimeException('Undefined method');
    }

    /** @inheritdoc  */
    public function getResolverRouteInstance(): RouteResolverInterface
    {
        return new RouteInstanceResolver;
    }

    /** @inheritdoc  */
    public function getResolverConnection(): ConnectionResolverInterface
    {
        return new ConnectionResolver(new ConnectionFactory, $this);
    }

    /** @inheritdoc  */
    public function getRoutesProvider(): RoutesProviderInterface
    {
        return new RoutesProvider;
    }
}
