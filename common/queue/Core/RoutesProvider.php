<?php

namespace common\queue\Core;


use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Routes\ImportStateUpdatingRoute;

/**
 * Class RoutesProvider
 *
 * @package common\queue\Core
 */
class RoutesProvider extends \Planet17\MessageQueueLibrary\Providers\RoutesProvider
{
    public function provideRouteClasses(): array
    {
        return [
            ImportStateUpdatingRoute::class
        ];
    }
}
