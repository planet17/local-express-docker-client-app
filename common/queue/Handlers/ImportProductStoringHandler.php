<?php

namespace common\queue\Handlers;

use common\interfaces\StoreProductImportingStatesMap;
use common\models\Databases\ActiveRecords\Stores\StoreProductImport;
use common\models\Databases\ActiveRecords\Stores\Product;
use Exception;
use InvalidArgumentException;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\ImportProductStoringDto;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\ImportStateUpdatingDto;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\ProductDto;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\ProductMakingExceptionDto;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Messages\ImportProductStoringMessage;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Routes\ImportProductStoringRoute;
use Planet17\MessageQueueLibrary\Handlers\BaseHandler;
use Planet17\MessageQueueLibrary\Interfaces\Messages\MessageInterface;
use Yii;
use yii\base\InvalidValueException;

/**
 * Class ImportProductStoringHandler
 *
 * @package common\queue\Handlers
 */
class ImportProductStoringHandler extends BaseHandler
{
    protected $routeClass = ImportProductStoringRoute::class;

    /**
     * Override handler.
     *
     * @param mixed|MessageInterface|ImportProductStoringMessage $payload
     */
    public function handle($payload): void
    {
        Yii::$app->db->close();
        Yii::$app->db->open();

        $dto = $payload->getDto();
        $model = $this->getImport($dto);
        if ($model === null) {
            throw new InvalidArgumentException('Import not found: ' . $dto->getImportIdentifier());
        }

        try {
            // throw exception from Service
            if ($dto->getItem() instanceof ProductMakingExceptionDto) {
                throw new InvalidValueException($dto->getItem()->getExceptionMessage());
            }

            if (!($dto->getItem() instanceof ProductDto)) {
                throw new InvalidArgumentException('Undefined type of item provided');
            }

            $modelProduct = Product::findByUpc($dto->getStoreIdentifier(), $dto->getItem()->getUpc());
            if ($modelProduct === null) {
                if (!$dto->getItem()->getTitle()) {
                    throw new InvalidValueException('Creating new item without title');
                }

                $modelProduct = new Product;
                $modelProduct->title = $dto->getItem()->getTitle();
                $modelProduct->store_id = $dto->getStoreIdentifier();
                $modelProduct->upc = $dto->getItem()->getUpc();
            }

            $modelProduct->price = $dto->getItem()->getPrice();
            $modelProduct->save();
        } catch (Exception $exception) {
            $model->incrementFail();
        }

        $model->incrementDone();
        $model->changeStateDoneByProduct();
        $model->save();
    }

    private function getImport(ImportProductStoringDto $dto): StoreProductImport
    {
        $model = StoreProductImport::find()
            ->where(
                [
                    'id' => $dto->getImportIdentifier(),
                    'store_id' => $dto->getStoreIdentifier()
                ]
            )->one();

        if (!$model) {
            throw new \LogicException('Invalid identifiers provided: #' . $dto->getImportIdentifier());
        }

        return $model;
    }
}
