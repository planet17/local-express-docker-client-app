<?php

namespace common\queue\Handlers;

use common\interfaces\StoreProductImportingStatesMap;
use common\models\Databases\ActiveRecords\Stores\StoreProductImport;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\ImportStateUpdatingDto;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Exceptions\InvalidTableException;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Messages\ImportStateUpdatingMessage;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Routes\ImportStateUpdatingRoute;
use Planet17\MessageQueueLibrary\Handlers\BaseHandler;
use Yii;

/**
 * Class ImportStateUpdatingHandler
 *
 * @package common\queue
 */
class ImportStateUpdatingHandler extends BaseHandler
{
    protected $routeClass = ImportStateUpdatingRoute::class;

    /**
     * Implement handle common.
     *
     * At each step it is looking for new tasks what could be started.
     *
     * @param ImportStateUpdatingMessage $payload
     *
     * @noinspection PhpMissingParamTypeInspection
     */
    public function handle($payload): void
    {
        Yii::$app->db->close();
        Yii::$app->db->open();
        $model = $this->getImport($payload->getDto());
        $this->resolveChangeState($payload->getDto(), $model);
    }

    private function getImport(ImportStateUpdatingDto $dto): StoreProductImport
    {
        $model = StoreProductImport::find()
            ->where(
                [
                    'id' => $dto->getImportIdentifier(),
                    'store_id' => $dto->getStoreIdentifier()
                ]
            )->one();

        if (!$model) {
            throw new \LogicException('Invalid identifiers provided: #' . $dto->getImportIdentifier());
        }

        return $model;
    }

    private function resolveChangeState(ImportStateUpdatingDto $dto, StoreProductImport $model)
    {
        if ($dto->getState() === StoreProductImportingStatesMap::IMPORT_STATE_NEW) {
            throw new \LogicException('Provided state not changed from initial: #' . $dto->getImportIdentifier());
        }

        if ($dto->getState() === StoreProductImportingStatesMap::IMPORT_STATE_PROCESSING) {
            $model->changeStateProcessing();
            if ($dto->getCountTotalProducts() && !$model->count_total) {
                $model->count_total = $dto->getCountTotalProducts();
            }
        }

        if ($dto->getState() === StoreProductImportingStatesMap::IMPORT_STATE_DONE) {
            if ($dto->getCountTotalProducts() && !$model->count_total) {
                $model->count_total = $dto->getCountTotalProducts();
            }

            if ($dto->getException()) {
                if ($dto->getException() instanceof InvalidTableException && $dto->getCountTotalProducts()) {
                    $model->count_total = $dto->getCountTotalProducts();
                    $model->count_processing_done = $dto->getCountTotalProducts();
                }

                $model->changeStateFailed();
            }

            $model->changeStateDone();
        }

        $model->save();
    }
}
