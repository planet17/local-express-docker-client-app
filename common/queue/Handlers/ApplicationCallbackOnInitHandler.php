<?php

namespace common\queue\Handlers;

use common\repositories\ImportRepositoryInterface;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\Balancer\ConfigurationDto;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\Balancer\TaskNewDto;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Messages\ImportProcessingBalancerMessage;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Routes\ApplicationCallbackOnInitRoute;
use LocalExpress\CommonQueueObjects\Bundles\ProcessManager\Messages\ProcessManagerMessage;
use Planet17\MessageQueueLibrary\Handlers\BaseHandler;
use Planet17\MessageQueueProcessManager\DTO\ProcessManagerDTO;
use Yii;

/**
 * Class ApplicationCallbackOnInitHandler
 *
 * @package common\queue\Handlers
 */
class ApplicationCallbackOnInitHandler extends BaseHandler
{
    protected $routeClass = ApplicationCallbackOnInitRoute::class;

    /** @var ImportRepositoryInterface $repository */
    private $repository;

    /** @var ConfigurationDto $configurationDto */
    private $configurationDto;

    /**
     * ApplicationCallbackOnInitHandler constructor.
     *
     * @param ImportRepositoryInterface $repository
     */
    public function __construct(ImportRepositoryInterface $repository, int $amount, int $limitPerCompany)
    {
        $this->repository = $repository;
        $this->configurationDto = new ConfigurationDto($amount, $limitPerCompany);
    }

    /**
     * Override.
     *
     * @inheritDoc
     */
    public function handle($payload): void
    {
        Yii::$app->db->close();
        Yii::$app->db->open();
        $this->dispatchConfiguration();

        foreach ($this->repository->all() as $row) {
            (new ImportProcessingBalancerMessage(
                new TaskNewDto(
                    $row['id'],
                    $row['store_id'],
                    $row['file_path'],
                    $this->configurationDto->getAmountDefaultProcessingParallelImportPerCompany()
                )
            ))->dispatch();
        }
    }

    /** @inheritDoc */
    public function initialize(): void
    {
        $this->dispatchConfiguration();
        parent::initialize();
    }

    /**
     * Method dispatching configuration messages via queue.
     */
    private function dispatchConfiguration()
    {
        (new ImportProcessingBalancerMessage($this->configurationDto))->dispatch();
    }
}
