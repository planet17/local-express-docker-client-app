<?php

namespace common\forms;


use yii\helpers\Html;

/**
 * Class ImportFileForm
 *
 * @package common\forms
 */
class ImportFileForm extends YiiHtmlBaseForm
{
    public function getPropertyFileName(): string
    {
        return $this->form
            ->field($this->model, $this->model::PROPERTY_NAME_FILE . '[]')
            ->fileInput(
            [
                'multiple' => true,
                'accept' => '.csv, .xls, .xlsx, text/csv, application/csv, text/comma-separated-values, application/csv, application/excel, application/vnd.msexcel, text/anytext, application/vnd. ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            ]
        );
    }

    public function getPropertyStore()
    {
        return $this->form
            ->field($this->model, $this->model::PROPERTY_NAME_STORE)
            ->dropdownList(
                $this->model->getDropdownOptionStores(),
                ['prompt' => 'Select store']
            );
    }

    public function getButtonSubmit(): string
    {
        return Html::submitButton('Submit', ['class' => 'btn btn-primary']);
    }

    public function __toString(): void
    {
        $this->begin();
        echo $this->getPropertyFileName();
        echo $this->getPropertyStore();
        echo $this->getButtonSubmit();
        $this->end();
    }
}
