<?php

namespace common\forms;

use common\interfaces\FormWidgetInterface;
use yii\base\Model;
use yii\base\Widget;
use yii\widgets\ActiveForm;

/**
 * Class BaseForm
 *
 * @package common\forms
 */
abstract class YiiHtmlBaseForm implements FormWidgetInterface
{
    /** @var ActiveForm $form */
    protected $form;

    /** @var Model $model */
    protected $model;

    public function __construct(Widget $form, Model $model)
    {
        $this->form = $form;
        $this->model = $model;
    }

    public function begin(): void
    {
        $this->form::begin($this->model->getWidgetConfiguration());
    }

    public function end(): void
    {
        $this->form::end();
    }
}
