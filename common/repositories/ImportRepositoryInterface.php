<?php

namespace common\repositories;

/**
 * Interface ImportRepositoryInterface
 *
 * @package common\repositories
 */
interface ImportRepositoryInterface
{
    /** @const DEFAULT_LIMIT */
    public const DEFAULT_LIMIT = 30;

    /**
     * Method return not more records than provided by limit value.
     *
     * It core method for getting results.
     *
     * @param int $limit
     * @param int $offset
     *
     * @return array
     */
    public function get(int $limit = self::DEFAULT_LIMIT, int $offset = 0): array;

    /**
     * Method return all records with preset conditions.
     *
     * It core method for getting results.
     *
     * @return array
     */
    public function all(): array;
}
