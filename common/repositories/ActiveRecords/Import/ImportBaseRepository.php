<?php

namespace common\repositories\ActiveRecords\Import;

use yii\db\ActiveQuery;
use common\models\Databases\ActiveRecords\Stores\Store;
use common\models\Databases\ActiveRecords\Stores\StoreProductImport;
use common\models\Databases\ActiveRecords\Stores\StoreProductImportState;
use common\repositories\ImportRepositoryInterface;

/**
 * Class ImportBaseRepository
 *
 * @package common\repositories\ActiveRecords\Import
 */
abstract class ImportBaseRepository implements ImportRepositoryInterface
{
    /** @inheritDoc */
    public function get(int $limit = self::DEFAULT_LIMIT, int $offset = 0): array
    {
        return $this->execute($limit, $offset);
    }

    /** @inheritDoc */
    public function all(): array
    {
        return $this->execute();
    }

    /**
     * Method implement preparing with concrete conditions ActiveQuery.
     *
     * @return ActiveQuery
     */
    abstract protected function getQueryWithFilters(): ActiveQuery;

    /**
     * Method return base query for all extended classes.
     *
     * Protected for using it at child\extended classes.
     *
     * @return ActiveQuery
     */
    protected function getActiveRecordQuery()
    {
        return StoreProductImport::find()
            ->select(
                [
                    StoreProductImport::tableName() . '.*',
                    Store::tableName() . '.title as store_name',
                    StoreProductImportState::tableName() . '.title as state_name',
                ]
            )
            ->leftJoin(
                Store::tableName(),
                StoreProductImport::tableName() . '.`store_id` = ' . Store::tableName() . '.`id`'
            )
            ->leftJoin(
                StoreProductImportState::tableName(),
                StoreProductImport::tableName() . '.`state_id` = ' . StoreProductImportState::tableName() . '.`id`'
            );
    }

    /**
     * Method just extract some common commands for execution.
     *
     * @return array
     */
    private function execute(int $limit = 0, int $offset = 0): array
    {
        $query = $this->getQueryWithFilters();
        if ($limit) {
            $query->limit($limit);
        }

        $query->offset($offset);

        return $query->asArray()->all();
    }
}
