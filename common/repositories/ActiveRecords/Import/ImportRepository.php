<?php

namespace common\repositories\ActiveRecords\Import;

use common\interfaces\StoreProductImportingStatesMap;
use yii\db\ActiveQuery;

/**
 * Class ImportProcessingRepository
 *
 * @package common\repositories\ActiveRecords\Import
 */
final class ImportRepository extends ImportBaseRepository
{
    /** @inheritDoc */
    protected function getQueryWithFilters(): ActiveQuery
    {
        return $this->getActiveRecordQuery()
            ->orderBy(['id' => 'DESC']);
    }
}
