<?php

namespace common\repositories\ActiveRecords\Import;

use common\interfaces\StoreProductImportingStatesMap;
use yii\db\ActiveQuery;

/**
 * Class ImportNewRepository
 *
 * @package common\repositories\ActiveRecords\Import
 */
final class ImportNewLastRepository extends ImportBaseRepository
{
    /** @inheritDoc */
    protected function getQueryWithFilters(): ActiveQuery
    {
        return $this->getActiveRecordQuery()
            ->where(['state_id' => StoreProductImportingStatesMap::IMPORT_STATE_NEW])
            ->orderBy(['created_at' => 'DESC']);
    }
}
