<?php

namespace common\repositories\ActiveRecords\Import;

use common\interfaces\StoreProductImportingStatesMap;
use yii\db\ActiveQuery;

/**
 * Class ImportProcessingRepository
 *
 * @package common\repositories\ActiveRecords\Import
 */
final class ImportProcessingRepository extends ImportBaseRepository
{
    /** @inheritDoc */
    protected function getQueryWithFilters(): ActiveQuery
    {
        return $this->getActiveRecordQuery()
            ->where(['state_id' => StoreProductImportingStatesMap::IMPORT_STATE_PROCESSING])
            ->orderBy(['updated_at' => 'ASC']);
    }
}
