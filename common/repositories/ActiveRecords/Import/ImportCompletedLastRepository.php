<?php

namespace common\repositories\ActiveRecords\Import;

use common\interfaces\StoreProductImportingStatesMap;
use yii\db\ActiveQuery;

/**
 * Class ImportDoneLastHourRepository
 *
 * @package common\repositories\ActiveRecords\Import
 */
final class ImportCompletedLastRepository extends ImportBaseRepository
{
    /** @inheritDoc */
    protected function getQueryWithFilters(): ActiveQuery
    {
        return $this->getActiveRecordQuery()
            ->where(['state_id' => StoreProductImportingStatesMap::IMPORT_STATE_DONE])
            ->andWhere(['count_fail' => 0])
            ->andWhere(['has_failed' => 0])
            ->orderBy(['done_at' => 'DESC', 'updated_at' => 'DESC']);
    }
}
