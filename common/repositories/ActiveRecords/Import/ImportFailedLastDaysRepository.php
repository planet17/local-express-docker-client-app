<?php

namespace common\repositories\ActiveRecords\Import;

use common\interfaces\StoreProductImportingStatesMap;
use yii\db\ActiveQuery;
use yii\db\conditions\OrCondition;

/**
 * Class ImportFailedRepository
 *
 * @package common\repositories\ActiveRecords\Import
 */
final class ImportFailedLastDaysRepository extends ImportBaseRepository
{
    /** @inheritDoc */
    protected function getQueryWithFilters(): ActiveQuery
    {
        return $this->getActiveRecordQuery()
            ->where(['state_id' => StoreProductImportingStatesMap::IMPORT_STATE_DONE])
            ->andWhere(new OrCondition([['>', 'count_fail', 0], ['has_failed' => true]]))
            ->orderBy(['done_at' => 'ASC']);
    }
}
