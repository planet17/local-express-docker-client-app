<?php

return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
    'user.passwordMinLength' => 8,
    'queue.service.store.productImport.parallelHandlers' => 2,
    /**
     * Don't change that value without improvent `service` application.
     *
     * Must improve safety of processing for duplicates items, or racing moments such update for still not created
     * products from previous document.
     */
    'queue.service.store.productImport.parallelHandlersPerCompany' => 1
];
