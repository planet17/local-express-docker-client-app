<?php

use yii\caching\FileCache;
use yii\db\Connection;

$host = getenv('DB_HOST') ?? '127.0.0.1';
$dbName =  getenv('DB_APP_NAME') ?? 'db_ui';

return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => FileCache::class,
        ],
        'db' => [
            'class' => Connection::class,

            'dsn' => "mysql:host={$host};dbname={$dbName}",
            'username' =>  getenv('DB_APP_USER') ?? 'root',
            'password' => getenv('DB_APP_PASS') ?? 'root_pass_word',
            'charset' => 'utf8',
        ],
    ],
];
