<?php

$connections = [
    (getenv('QUEUE_CONNECTION_NAME') ?: 'default') => [
        'driver' => getenv('QUEUE_DRIVER'),
        'host' => getenv('QUEUE_HOST'),
        'port' => getenv('QUEUE_PORT'),
    ]
];

/**
 * Run turn on configuration for Queue app.
 *
 * @var string $connectionName
 * @var array $config
 */
foreach ($connections as $connectionName => $config) {
    \common\queue\Core\ConnectionManager::getInstance()
        ->addConnection($config, $connectionName)
        ->bootRoutes();
}
