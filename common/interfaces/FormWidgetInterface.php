<?php

namespace common\interfaces;

use yii\base\Model;
use yii\base\Widget;

/**
 * Interface FormWidgetInterface
 *
 * @package common\interfaces
 */
interface FormWidgetInterface
{
    /**
     * FormWidgetInterface constructor.
     *
     * @param Widget $widget
     * @param Model $model
     */
    public function __construct(Widget $widget, Model $model);

    /**
     *
     */
    public function begin(): void;

    /**
     *
     */
    public function end(): void;
}
