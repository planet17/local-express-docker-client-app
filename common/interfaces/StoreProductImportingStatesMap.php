<?php

namespace common\interfaces;

/**
 * Interface StoreProductImportStateMap
 *
 * @package common\interfaces
 */
interface StoreProductImportingStatesMap
{
    public const IMPORT_STATE_NEW = 1;
    public const IMPORT_STATE_PROCESSING = 2;
    public const IMPORT_STATE_DONE = 3;
}
