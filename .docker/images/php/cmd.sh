#!/bin/sh

# Below commands uses for asynchronyous running queue handler for current project.
# Could be replaced by supervisord or some another way, but it is the simplest way for it.
./yii queue-store-import-product/run-handler-callback &
./yii queue-store-import-product/run-handler-import-product-storing &
./yii queue-store-import-product/run-handler-import-state-updating &

# Core command:
php-fpm
