<?php

use yii\db\Migration;

/**
 * Class m210118_203833_create_store_products
 */
class m210118_203833_create_store_products extends Migration
{
    public const TABLE_NAME = '{{%store_products}}';

    /** @const TABLE_NAME_STORES Value from m210118_203814_create_stores::TABLE_NAME */
    public const TABLE_NAME_STORES = '{{%stores}}';

    public const LENGTH_FIELD_UPC = 10;

    public const LENGTH_FIELD_TITLE = 64;

    public const FK_INDEX_NAME_STORE = 'fk_sp_stores_id';

    public const INDEX_NAME_UPC = 'idx_sp_upc';

    /**
     * Getter method for table name.
     *
     * @return string
     */
    private function getTable(): string
    {
        return self::TABLE_NAME;
    }

    /** @inheritDoc */
    public function up(): bool
    {
        $this->createTable(
            $this->getTable(),
            [
                'id' => $this->primaryKey(),
                'store_id' => $this->integer()->notNull(),
                'upc' => $this->string(self::LENGTH_FIELD_UPC)
                    ->notNull(),
                'title' => $this->string(self::LENGTH_FIELD_TITLE)
                    ->notNull(),
                'price' => $this->decimal(10, 2)
            ]
        );

        $this->addForeignKey(
            self::FK_INDEX_NAME_STORE,
            $this->getTable(),
            'store_id',
            self::TABLE_NAME_STORES,
            'id',
            'CASCADE'
        );

        $this->createIndex(
            self::INDEX_NAME_UPC,
            $this->getTable(),
            ['store_id', 'upc'],
            true
        );

        return true;
    }

    /** @inheritDoc */
    public function down(): bool
    {
        $this->dropForeignKey($this->getTable(), self::FK_INDEX_NAME_STORE);
        $this->dropIndex($this->getTable(), self::INDEX_NAME_UPC);
        $this->dropTable($this->getTable());

        return true;
    }
}
