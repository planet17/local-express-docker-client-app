<?php

use common\interfaces\StoreProductImportingStatesMap;
use yii\db\Migration;

/**
 * Class m210118_211713_create_store_product_import
 */
class m210118_211713_create_store_product_import extends Migration
{
    public const TABLE_NAME = '{{%store_product_import}}';

    /** @const TABLE_NAME_STORES Value from m210118_203814_create_stores::TABLE_NAME */
    public const TABLE_NAME_STORES = '{{%stores}}';

    /** @const TABLE_NAME_STORES Value from m210118_210849_create_store_product_import_states::TABLE_NAME */
    public const TABLE_NAME_STORE_PRODUCT_IMPORT_STATES = '{{%store_product_import_states}}';

    public const LENGTH_FIELD_FILE_NAME = 64;

    public const LENGTH_FIELD_FILE_PATH = 72;

    public const FK_INDEX_NAME_STORE = 'fk_spi_stores_id';

    public const FK_INDEX_NAME_STATE = 'fk_spi_store_product_import_states_id';

    /** @inheritDoc */
    public function up(): bool
    {
        $this->createTable(
            $this->getTable(),
            [
                'id' => $this->primaryKey(),

                'file_path' => $this->string(self::LENGTH_FIELD_FILE_PATH),

                'file_name_origin' => $this->string(self::LENGTH_FIELD_FILE_NAME),

                'store_id' => $this->integer()
                    ->notNull(),

                'state_id' => $this->integer()
                    ->notNull()
                    ->defaultValue(StoreProductImportingStatesMap::IMPORT_STATE_NEW),

                'has_file_been_read' => $this->boolean()->defaultValue(false),
                'has_failed' => $this->boolean()->defaultValue(false),

                'count_total' => $this->integer()->defaultValue(0),
                'count_fail' => $this->integer()->defaultValue(0),
                'count_processing_done' => $this->integer()->defaultValue(0),

                'created_at' => $this->timestamp()
                    ->notNull()
                    ->defaultExpression('CURRENT_TIMESTAMP'),

                'done_at' => $this->timestamp()
                    ->null(),

                'updated_at' => $this->timestamp()
                    ->notNull()
                    ->defaultExpression('CURRENT_TIMESTAMP')
                    ->append('ON UPDATE CURRENT_TIMESTAMP'),
            ]
        );

        $this->addForeignKey(
            self::FK_INDEX_NAME_STORE,
            $this->getTable(),
            'store_id',
            self::TABLE_NAME_STORES,
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            self::FK_INDEX_NAME_STATE,
            $this->getTable(),
            'state_id',
            self::TABLE_NAME_STORE_PRODUCT_IMPORT_STATES,
            'id',
            'CASCADE'
        );

        return true;
    }

    /** @inheritDoc */
    public function down(): bool
    {
        $this->dropForeignKey($this->getTable(), self::FK_INDEX_NAME_STORE);
        $this->dropForeignKey($this->getTable(), self::FK_INDEX_NAME_STATE);
        $this->dropTable($this->getTable());

        return true;
    }

    /**
     * Getter method for table name.
     *
     * @return string
     */
    private function getTable(): string
    {
        return self::TABLE_NAME;
    }
}
