<?php

use yii\db\Migration;

/**
 * Class m210118_203814_create_stores
 */
class m210118_203814_create_stores extends Migration
{
    public const TABLE_NAME = '{{%stores}}';

    public const LENGTH_FIELD_TITLE = 32;

    /**
     * Getter method for table name.
     *
     * @return string
     */
    private function getTable(): string
    {
        return self::TABLE_NAME;
    }

    /** @inheritDoc */
    public function up(): bool
    {
        $this->createTable(
            $this->getTable(),
            [
                'id' => $this->primaryKey(),
                'title' => $this->string(self::LENGTH_FIELD_TITLE)
                    ->notNull()
                    ->unique()
            ]
        );

        return true;
    }

    /** @inheritDoc */
    public function down(): bool
    {
        $this->dropTable($this->getTable());

        return true;
    }
}
