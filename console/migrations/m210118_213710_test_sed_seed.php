<?php

use common\models\Databases\ActiveRecords\Stores\Product;
use common\models\Databases\ActiveRecords\Stores\Store;
use common\models\Databases\ActiveRecords\Auth\User;
use yii\base\InvalidConfigException;
use yii\db\Migration;

/**
 * Class m210118_213710_test_sed_seed
 */
class m210118_213710_test_sed_seed extends Migration
{
    public const LENGTH_FIELD_STORE_TITLE = 32;
    public const LENGTH_FIELD_PRODUCT_TITLE = 64;
    public const STORES_AMOUNT = 10;
    public const STORES_AMOUNT_WITHOUT_PRODUCTS = 5;
    public const PRODUCT_MAX_AMOUNT_PER_STORE = 500;

    private $stores = [];

    /** @var \Faker\Generator|mixed */
    private $faker;

    /**
     * {@inheritdoc}
     * @throws InvalidConfigException|\yii\db\Exception
     * @throws Exception
     */
    public function safeUp(): bool
    {
        $this->faker = Faker\Factory::create();
        $this->fillUsers();
        $this->fillStores();
        $this->fillProducts();

        return true;
    }

    private function fillUsers(): void
    {
        $user = new User;
        $user->username = 'system';
        $user->email = 'system@social-express.com';
        $user->setPassword('admin');
        $user->generateAuthKey();
        $user->status = User::STATUS_ACTIVE;
        $user->save();
    }

    private function fillStores(): void
    {
        for ($i = self::STORES_AMOUNT; $i--;) {
            $store = new Store;
            do {
                $word = $this->faker->company;
            } while (self::LENGTH_FIELD_STORE_TITLE < strlen($word));
            $store->title = $word;
            $store->save();

            $this->stores[] = $store;
        }
    }

    /** @throws Exception */
    private function fillProducts(): void
    {
        if (self::STORES_AMOUNT_WITHOUT_PRODUCTS >= self::STORES_AMOUNT) {
            return;
        }

        $stores = array_slice($this->stores, 0, -self::STORES_AMOUNT_WITHOUT_PRODUCTS);
        foreach ($stores as $store) {
            for ($i = random_int(1, self::PRODUCT_MAX_AMOUNT_PER_STORE); $i--;) {
                $product = new Product();
                do {
                    $word = $this->faker->numerify('0101######');
                } while (Product::findByUpc($store->id, $word));
                $product->upc = $word;
                do {
                    $word = $this->faker->word();
                } while (self::LENGTH_FIELD_PRODUCT_TITLE < strlen($word));
                $product->title = $word;
                $product->price = $this->faker->randomFloat(2, 1, 1000);
                $product->store_id = $store->id;
                $product->save();
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown(): bool
    {
        $this->truncateTable('{{%store_products}}');
        $this->truncateTable('{{%store_product_import}}');
        $this->truncateTable('{{%stores}}');
        $this->truncateTable('{{%users}}');

        return true;
    }
}
