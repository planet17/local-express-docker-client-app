<?php

use yii\db\Migration;

/**
 * Class m210118_210849_create_store_product_import_states
 */
class m210118_210849_create_store_product_import_states extends Migration
{
    public const TABLE_NAME = '{{%store_product_import_states}}';

    public const LENGTH_FIELD_TITLE = 10;

    /**
     * Getter method for table name.
     *
     * @return string
     */
    private function getTable(): string
    {
        return self::TABLE_NAME;
    }

    /**
     * @inheritDoc
     * @throws \yii\db\Exception
     */
    public function up(): bool
    {
        $this->makeTable();
        $this->fillTheTable();

        return true;
    }

    /**
     * Method contains settings for create table.
     */
    private function makeTable(): void
    {
        $this->createTable(
            $this->getTable(),
            [
                'id' => $this->primaryKey(),
                'title' => $this->string(self::LENGTH_FIELD_TITLE)
                    ->notNull()
                    ->unique()
            ]
        );
    }

    /**
     * Method insert starter records into the table.
     *
     * @throws \yii\db\Exception
     */
    private function fillTheTable(): void
    {
        Yii::$app->db
            ->createCommand()
            ->batchInsert(
                $this->getTable(),
                ['id', 'title'],
                [
                    [1, 'NEW'],
                    [2, 'PROCESSING'],
                    [3, 'DONE'],
                ]
            )->execute();
    }

    /** @inheritDoc */
    public function down(): bool
    {
        $this->dropTable($this->getTable());

        return true;
    }
}
