<?php

namespace console\controllers;

use common\queue\Core\ConnectionManager;
use common\queue\Handlers\ApplicationCallbackOnInitHandler;
use common\queue\Handlers\ImportProductStoringHandler;
use common\queue\Handlers\ImportStateUpdatingHandler;
use common\repositories\ActiveRecords\Import\ImportNewRepository;
use common\repositories\ActiveRecords\Import\ImportProcessingRepository;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\Balancer\ConfigurationDto;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\Balancer\TaskNewDto;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Messages\ImportProcessingBalancerMessage;
use Yii;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * It contains commands for run handlers working with service.
 *
 * @package console\controllers
 */
class QueueStoreImportProductController extends Controller
{
    /**
     * Handler for 1) set-up config for import handlers at service; 2) it resend tasks of new import to the Service.
     *
     * Run it by following:
     *
     * ```shell
     * ./yii queue-store-import-product/run-handler-callback
     * ```
     */
    public function actionRunHandlerCallback()
    {
        (new ApplicationCallbackOnInitHandler(
            new ImportNewRepository,
            Yii::$app->params['queue.service.store.productImport.parallelHandlers'],
            Yii::$app->params['queue.service.store.productImport.parallelHandlersPerCompany']
        ))->initialize();
    }

    /**
     * Handler for updating/creating products had been processed from import file.
     *
     * Run it by following:
     *
     * ```shell
     * ./yii queue-store-import-product/run-handler-import-product-storing
     * ```
     */
    public function actionRunHandlerImportProductStoring()
    {
        (new ImportProductStoringHandler)->initialize();
    }

    /**
     * Handlers for save new state of importing task.
     *
     * Run it by following:
     *
     * ```shell
     * ./yii queue-store-import-product/run-handler-import-state-updating
     * ```
     */
    public function actionRunHandlerImportStateUpdating()
    {
        (new ImportStateUpdatingHandler)->initialize();
    }

    /**
     * Handlers for save new state of importing task.
     *
     * Run it by following:
     *
     * ```shell
     * ./yii queue-store-import-product/run-re-processing
     * ```
     */
    public function actionRunReProcessing()
    {
        foreach ((new ImportProcessingRepository)->all() as $row) {
            (new ImportProcessingBalancerMessage(
                new TaskNewDto(
                    $row['id'],
                    $row['store_id'],
                    $row['file_path'],
                    Yii::$app->params['queue.service.store.productImport.parallelHandlersPerCompany']
                )
            ))->dispatch();
        }
    }

    /**
     * Handlers for save new state of importing task.
     *
     * Run it by following:
     *
     * ```shell
     * ./yii queue-store-import-product/run-another-parallel-amount-handlers
     * ```
     */
    public function actionRunAnotherParallelAmountHandlers()
    {
        (new ImportProcessingBalancerMessage(new ConfigurationDto(
            0,
            Yii::$app->params['queue.service.store.productImport.parallelHandlersPerCompany']
        )))->dispatch();
    }
}
